# Semaine 2

Cette semaine nous avons commencez par un brainstorming avec Terry sur différentes idées que nous pourrions incorporer dans notre projet suivi d'une première visite du musée. L'après-midi, les étudiants du module 1 sont venus au musée et, après une introduction, nous leur avons donné une visite du musée.

## Brainstorming 

Nous avons commecé par parler de MuseumX qui est un workshop se passant une fois par an dans un musée sélectionné où plusieurs équipes de volontaires ont 3 jours (de vendredi à dimanche) pour créer une nouvelle interface pour le musée. La meilleure idée est sélectionnée par un jury et sera réalisée par le musée.

Ci-dessous, une liste d'idées que nous avons partagé avec la table :

- Un jeu style "lego" où les enfants peuvent assembler leurs objets;
- Attirer l'attention avec des jeux;
- Avec des pièces détachées, l'enfant doit inventer un meuble
- ... Jouer au designer;
- Créer un mini-moule, plasticine ou pâte que l'on met dans le four et sortir avec son objet du musée;
- Toy-story : les objets parlent sur comment ils ont été imaginés -> animer les objets;
- Chasse au trésor avec des boites qui contiennent des échantillons expliquant l'objet, tout se passe à un endroit à la fois avec une narration/fil conducteur;
- Exemple venant du musée des égouts (à anderlecht) : audio-guide avec 2 personnages qui se parlent. Tout se passe entre eux et ils posent des questions, interactif, on répond et on apprend;
- Plasticotek: développement entier d'un objet... évolution de la matière;
- L'enfant se photographie avec l'objet -> décor ou filtre instagram
- Parler du designer;
- Exemple venant d'une exposition sur Stark: Les objets sont recouverts d'un tissus et les visiteurs ont un walkman où ils entendent Stark parler lorsqu'il montre et explique l'objet;
- Voir la déconstrustrution des objets;
- Apprendre à l'enfant comment comprendre les objets.

## Visite du musée





## Virtual reality

En fin de journée, j'ai utilisé une application se nommant "3d scanner app" me permettant de scanner une pièce ou un objet à l'aide de l'appareil photo et du lidar de ma tablette. J'ai ici scanné la seconde salle du musée où se trouve des objets inspirés de la conquête spatiale dans les années 70. 
Ceci a comme but d'une part, de tester l'application et ce qu'il est possible de faire avec et d'autre part, ces donnéês me permettent de créer ma scène en réalité augmentée en fonction de l'espace "réel" avec lequel je peux venir jouer.

![Salle 2 - 3D scanned](../images/Salle2_3d.jpeg)
