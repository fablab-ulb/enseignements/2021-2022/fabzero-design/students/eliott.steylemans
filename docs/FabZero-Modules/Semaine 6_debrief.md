# Debrief semaine 4 (jeudi)

## Résumé visite des enfants

| Ce qui a marché       | Ce qui n'a pas marché           |
| ------------- |:-------------:|
| C'est quoi le design? sac à dos, usage, signe    | Inventer des projets |
| Liens avec ce qu'ils connaissent (chantilly -> impression 3d)     | Nom des objets     |
| Points communs / différences | Durée de la visite     |
| Dessins des chaises |      |
| Déconstruire les objets |      |
| Même hauteur avec les enfants |      |
| Comparaison |      |
| Mémoire des enfants |      |


## Réflexion sur les audio-guide



| Analyse audio-guide       |          |
| ------------- |:-------------:|
| Banksy       | Une minute en moyenne par oeuvre, déconstruction de l'oeuvre ainsi que son contexte et apport d'explications, très compliqué pour un enfant         |
| Kaserne      | Lenteur dans la narration à cause de terme très compliqué         |
| Focus on furniture        | Enregistrement de mauvaise qualité         |

Remarques négatives générales:
- Pas de narration et aucun fil conducteur, on passe d'une oeuvre à une autre en arretant à chaque fois l'audio-guide et en devant indiquer à nouveau le numéro de l'oeuvre
- Compliqué de remettre l'objet dans son contexte
- Language trop compliqué pour un enfant
- Aucune interraction avec le visiteur
