# Semaine projet

Malheureusement, cette semaine - dû à une semaine projet organisée par l'ULB - je n'ai pas pu assister à la visite du musée avec les enfants de Decroly.

Suite à cette semaine intensive, j'ai pu me concentrer à nouveau sur ce projet de design.

## Concept art des personnages

Cette semaine, j'ai effectué un premier jet sur des idées de personnages. Dans mon interface en réalité augmentée, deux personnages racontront une histoire tout en transmettant les diverses informations concernant les objets et le musée. 

Ma vision dans la création des personnages est de créer des protagonistes qui seront attrayant pour les enfants tout en prennant en compte les contraintes techniques auxquelles je ferai façe lors de la création de ceux-ci.

Les critères de mes personnages sont:
- Une taille adaptée pour intéragir avec des enfants
- Attrayant et mignon qui donne confiance à l'enfant
- Limiter les parties mobile afin d'éviter un grand travail d'animation
- Visage en deux dimensions, à nouveau pour limiter le travail d'animation

Ci-suivant mes premiers concept arts de robots:
<p float="middle">
  <img src="../images/Concept_robot_v1_01.jpeg" width="330" />
  <img src="../images/Concept_robot_v1_02.jpeg" width="330" /> 
  <img src="../images/Concept_robot_v1_03.jpeg" width="330" />
</p>

Enfin, voici les deux robots que j'ai sélectionnés pour cette première version:

<p float="middle">
  <img src="../images/Concept_robot_v1_06.jpeg" width="500" />
  <img src="../images/Concept_robot_v1_07.jpeg" width="500" /> 
</p>

Une petite vidéo montrant l'évolution du robot "A" lorsque je le dessinais:

![RobotA_v01](../images/Concept_robot_v1_A.GIF)


## Application 

J'ai également commencé à développer mon application et uploadé le code source dans un nouveau repository que vous retrouverez [Ici](https://gitlab.com/eliottst/Museum)(est pour l'instant en privé).
Dans le cas où j'aurais besoin d'aide, ce repository permettra à la personne en question d'accèder directement au code et d'y proposer des modifications.

L'application sera composée en deux sections avec une page d'acceuil introduisant le fonctionnement de l'application et les règles à respecter dans le musée et, par la suite, une seconde section où se trouvera l'experience en AR.

### Première version de la page d'acceuil
<p float="middle">
  <img src="../images/Simulator_Screen_walkthrough_v1_1.png" width="330" />
  <img src="../images/Simulator_Screen_walkthrough_v1_2.png" width="330" /> 
  <img src="../images/Simulator_Screen_walkthrough_v1_3.png" width="330" />
</p>

## Storyboard

Ci-dessous, un première exemple d'histoire reprennant nos deux protagonistes qui expliquent les fauteuils gonflables.


<p float="middle">
  <img src="../images/Concept_storyboard_v1_01.PNG" width="500" />
  <img src="../images/Concept_storyboard_v1_02.PNG" width="500" /> 
  <img src="../images/Concept_storyboard_v1_03.PNG" width="500" />
  <img src="../images/Concept_storyboard_v1_04.PNG" width="500" />
  <img src="../images/Concept_storyboard_v1_05.PNG" width="500" />
  <img src="../images/Concept_storyboard_v1_06.PNG" width="500" />
</p>

![Storyboard_Gonflable](../images/Concept_storyboard_v1_A.GIF)


