# Semaine 4

Cette semaine, le professeur de la classe de l'école primaire de Decroly, Yan, est venu nous rendre visite au musée avec Steph et Flo du Steamlab. Nous leurs avons tout d'abord, avec l'aide de Terry, donné une visite du musée. Ceux-ci ne s'étaient pas renseignés au préalable sur le musée et c'était à nous de leur donner une visite la plus complète possible afin qu'ils aient une bonne appréhenstion sur quoi nous allons travailler.

Ensuite, nous avons discuté - tous ensembles - d'idées pour adapter cette visite aux enfants.
Voici les différents points que j'ai noté lors de cette conversation:

- Fil conducteur
- Date des objets
- Attention perdue si on raconte aux enfants quelques choses qu'ils ne peuvent pas suivre
- Coup de coeur pour la chaise frigo
- Un objet remarquable par salle pour expliquer un concept ou une anecdote 
- Faire des liens concrets avec ce qu'ils connaissent
- Objets qui surprennent
- Anecdote: 1êre chaise Pantone qui casse si on s'assoie dessus
- Permettre à l'enfant de comprendre la déconstruction d'un objet / comment le designer crée
- Ils doivent essayer de comprendre pourquoi l'objet est comme ça


Pour terminer, nous avons fait une sélection de ces objets remarquable:

*note: pseudo des objets et non pas leurs réels noms*

- Frigo recyclé
- Dyson // chaise 3D
- Tabouret/vase
- Caillou / Pratone
- Canapé gonfalble
- Phantom
- Sphère d'isolement // Dondolo
- Panton // Floris de Gunter // homme
- Dent jaune
- Tvs // lampes // formes qui utilisent le même moule
- Vêtements en nylon
