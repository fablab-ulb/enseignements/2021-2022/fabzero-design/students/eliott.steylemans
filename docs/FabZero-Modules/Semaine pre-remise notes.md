## Transition du projet vers une nouvelle platforme

Bien que nos démos ont "fonctionnées" jusqu'à lors très bien avec reality composer, nous nous sommmes rendu compte que la platforme reste limitée en beaucoup de points et ne peut pas être utilisée pour projet de notre envergure. 
Les défauts notables que nous avons relevés sont :
- Les problèmes que nous avons eu lors du pré-jury;
- Fonctionne uniquement sur iOS (iPad+iPhone) ;
- Tracking limité (tracking des objets particulièrement difficiles lorsque ceux-ci réfléchissent de la lumière)  ;
- La transition entre chaque objets de la visite n'est pas possible (pour cause, encore une fois, du tracking limité. Chaque utilisateur devrait aligner de manière manuelle la scène. Ce qui n'est pas souhaitable) ;
- Interactions limitées et l'apport de nouvelles visites, fonctionnalités,... compliquées.

Pour toutes ces raisons, nous avons pris la décision de transitionner le projet vers un nouveau moteur afin de fournir un projet final qui pourrait être réalisé immédiatement et qui permettrait de mieux convaincre de potentiels investisseurs.

La platforme sur laquelle nous allons maintenant travailler est Unity. Unity est un moteur de jeu multi-platforme et est l'un des plus utilisés dans l'industrie du jeu vidéo. 
Unity va nous permettre de résoudre tout les problèmes évoqués ci-dessus, d'obtenir une meilleure flexibilité de conception et d'évoluer le projet en même rythme que nos idées.

## Choix du Framework

Un Framework est un logiciel qui est développé par et pour les développeurs pour créer des applicatioins. Autrement-dit, c'est une base sur lequel s'appuie le développeur lui permettant de se concentrer sur des tâches plus importantes.

Dans notre cas, nous devons sélectionner le Framework qui va être la base de fonctionnement pour notre expérience de réalité augmentée.

........
AR Foundation
Vuforia

## Pour aller plus loin...

Le framework utilisé peut tout à fait convenir pour l'expérience finale et est un très bon choix pour faciliter la vie du développeur grâce à sa bonne documentation, ses outils mis à disposition et sa compatibilité avec les différents appareils de réalité augmentée.
Toute fois, Vuforia reste payant (le prix m'est inconnu, une demande de devis est nécessaire pour en savoir plus) et celui-ci reste limité à un usage local avec l'application du musée à installer pour chaque utilisateur.

Il est peut être préférable, alors, de choisir une solution gratuite et qui permettrait dans un futur de se lancer sans le besoin de télécharger l'application spécifique. Il est en effet possible d'utiliser ce qu'on appelle des "Cloud anchors" qui permet à l'appareil AR de reconnaitre le lieu où il se trouve et de lancer l'expérience qui lui est attaché. Ces "cloud anchors" permettent également de synchroniser les expériences avec plusieurs utilisateurs. On pourrait alors y ajouter une fonction qui permettrait de lancer la visite pour un groupe et ceux-ci verront tous la même chose au même moment et peuvent intéragir entre eux.

Pour ce qui en est des visites, l'utilisation de Unity nous permet maintenant très facilement de rajouter de nouvelles animations, interactions,.. ou de les modifier. 

Pour finir, l'intégration de l'expérience avec l'application peut être résolue soit en payant la license Vuforia, soit en utilisant les "Cloud anchors" présentés ci-dessus.