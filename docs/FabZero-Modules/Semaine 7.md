# Production de l'expérience AR


Tout d'abord, nous avons réfléchi sur notre angle d'attaque sur ce que nous allions transmettre aux enfants lors de la visite au musée. Nous avons fait le choix de leur apprendre le passage d'un objet de base (tel, à titre d'exemple, une bouteuille d'eau) à un objet confectionné par un designer. Nous devons alors transmettre aux enfants le cheminement du designer et les choix qu'il a effectué afin d'atteindre son objectif pour concevoir son objet. 

Nous avons également du réfléchir au moyen dont nous allons transmettre ces informations. Nous nous sommes tournés vers la réalité augmentée. Ce moyen est, pour nous, l'évolution du guide audio que nous retrouvons aux musées.

Après avoir défni que nous allions travailler en réality augmentée, il était temps de concevoir notre idée et celle-ci s'est faite en plusieurs étapes.

## Création de l'histoire

Dans cette première étape, nous avons écrit un script pour le premier objet dont nous allons parler. Cet objet est la chaise Panton, de Verner Panton (1959).

![Script - Panton](../images/Script_Pantone.png)


## Aspects technique

Pour concevoir le projet, nous avons mené une recherche sur les logiciels que nous allions utilisé pour la modélisation et l'animation ainsi que le platforme sur laquelle l'expérience en réalité fonctionnera.

Nous avons décidé ici d'utiliser Blender comme logiciel principal. Blender est un logiciel Open Source, disponible sur MacOS, Windows et Linux et permet de faire un grand nombre de choses tel que la modelisation et l'animation qui nous intéresse. Nous l'avons selectionné car il est gratuit et donc accessible à tous, permet de faire tout ce que l'on a besoin et parce que j'ai, personnellement, eu l'occasion de l'utiliser pour différents projet et j'ai donc déjà acquéri une base qui me permet de mieux comprendre ce qu'il est nécessaire de faire et le temps que les différentes tâches pourraient nous prendre.

Pour ce qu'il en est de l'AR, plusieurs framework existent tels que ARkit, ARCore, HoloLens ou encore MagicLeap pour citer les plus grands. 
Nous avons fait le choix d'utiliser ARkit fourni par Apple. Le choix de travailler uniquement sur ARKit est car Apple a mis en place différents outils comme Reality Composer qui nous permet de créer une expérience en AR sans à coder les différentes actions que nous mettrions dans cette expérience. Ou encore ARkit Scanner qui nous permet de scanner un objet en 3 dimensions pour pouvoir anchrer notre scéne à un objet de la réalité afin de simplifier la vie de l'utilisateur et assurer que tout se positionne correctement. Ce framework est uniquement compatible avec des appareils iOS.

## Modélisation

Comme je l'ai préalablement évoqué, la modélisation s'est passée au travers de Blender. La forme de départ pour modéliser Plastico était un cube. J'ai placé le concept art de Plastico en fond et j'ai ensuite manipulé ce cube de base pour y obtenir la forme finale de Plastico. J'y ai ensuite ajouté ses bras, son visage et ses accessoires.
Pour ce qu'il en est des chaises, la chaise "Pantone" peut être trouvé sur le site officiel de Vitra et les chaises d'écoles sont des modèles (CC Attribution) trouvées sur [Sketchfab](https://sketchfab.com/3d-models/school-chair-d77a0e107a764dff9be2b1d23fd352db).

![Plastico](../images/Plastico_render.png)


## Animation

Afin de pouvoir animer Plastico, nous devons lui faire un "squelette" ou une armature. 
Ceci permet de controler notre personnage bien plus aisément et permet également de ne pas modifier l'objet en lui même. Ceci permet également de connecter les différents "os" tout au long du personnage et d'avoir des jonctions plus naturelles ainsi qu'un travail d'animation plus léger. Exemple: lorsque nous glissons notre genou vers un coté, notre bassin, jambe et cheville pivote avec pour accompagner le mouvement. Ce type de mouvement peut être controllé grâce à notre armature.

Les objets sont, quant à eux, directement animés sans avoir besoin de créer un squelette.

<p float="middle">
  <img src="../images/Plastico_rig1.png" width="500" />
  <img src="../images/Plastico_rig2.png" width="500" /> 
</p>

![Plastico - Animation](../images/Plastico_animation1.png)

Difficultés qui persistent:
- Impossibilité de faire apparaitre de nouveaux objets, il faut alors les placer en dehors du champs de vision de l'utilisateur et le déplacer quand il est temps de l'apercevoir 
- Déformation des les bras de Plastico lorsque nous essayons de rendre son bras extensile afin qu'il puisse prendre des objets en mains
- Certains objets ne se positionnent pas correctement dans la scène

## Intégration dans Reality Composer 

Reality Composer est une application développée par Apple afin de créer des expériences AR sans à devoir coder. Bien entendu, l'application est alors limitée par le fait qu'aucun code n'est nécessaire à créer une expérience mais elle en reste tout de même fournie en action que l'on peut réaliser et répond amplement à nos besoins. Cette application est disponible sur MacOS, iOS et iPadOS. Ce qui permet de travail sur son appareil mobile et d'y avoir un aperçu en temps réel.

![Reality Composer](../images/Screenshot_RealityComposer1.png)

Seulement, les objets et animations que Reality Composer doivent être dans un format .USDZ (Pixar’s Universal Scene Description standard) qui est un très bon format de fichier reprennant Objets, textures et animations tout en un. Cependant, Blender ne permet pas d'exporter directement notre scène en .usdz. Il me faut alors l'exporter en .gltf et transverser le fichier dans Reality Converter - autre outil développé par Apple pour convertir certains format de fichier en .usdz - pour ensuite pouvoir l'importer dans notre projet Reality Composer.

![Reality Converter](../images/Screenshot_RealityConverter.png)

Une fois que tout est positionné où nous le souhaitons, il ne faut plus que l'exporter sur notre téléphone et celui-ci se charge du reste.



## Résultats du test au fablab

Nous avons eu l'occasion de tester notre projet au fablab où les étudiants (6-7 ans) de l'école Decroly sont venus afin d'essayer nos projets respectifs.
Dans l'ensemble, nous sommes très content des résultats que nous avons eus. Les enfants étaient concentrés lors de la visite, se sont amusés (nous avons eu droit à quelques rires) et ont extrèmement bien retenu ce que nous leurs avons transmis. Si bien que le jeu que nous avions prévu après la visite afin de les questionners sur des sujets similaires pour approfondir le sujet (voir le Git de Zoé et/ou Camille pour plus d'informations) n'était en fait pas du tout nécessaire car les enfants savaient tout nous retransmettre même avant de commencer ce dernier.

Ce que nous avons également remarqué au fur et à mesure de la journée, est que les enfants veulent avoir un petit souvenir du musée et de Plastico. Nous avons alors pensé à la création d'une "Sharing Box" à la fin de la visite permettant de prendre une photo souvenir avec Plastico et des objets du musée.

<p float="middle">
  <img src="../images/Photo-fablab1.JPG" width="350" />
  <img src="../images/Photo-fablab2.JPG" width="350" /> 
  <img src="../images/Photo-fablab3.JPG" width="350" /> 
</p>
