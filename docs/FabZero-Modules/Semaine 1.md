# Semaine 1

Cette semaine nous sommes allés pour la première fois au musée du design de Bruxelles. Musée autour duquel nous allons travaillés tout ce quadrimèstre à la création d'un nouveau média pour les visites d'enfants agés de 6 à 12 ans.

Après une exposition de plus de 2000 objets en plasitque en 2009, la fondation Roi Baudoin décide d'acheter la collection afin de l'exposer de manière permanente au musée du design qui ouvre officiellement ses portes en 2015.


## Plasticoteque

Nous retrouvons trois grandes familles de plastiques:

- Les thermoplastiques
- Les thermodurcissables
- Les élastomères

Ensuite, nous avons cinq procédés de fabrication :

- Injection
> Technique qui consisite à injecter du plastique dans un moule qui donnera la forme de l''objet. On y retrouve ainsi un petit cercle où se trouvait le point d'injection du plastique. 
> Technique la plus populaire qui permet d'avoir un cucle de production très rapide.

- Extrusion
> Des profilés continus sortent de production et on le coupe pour donner leur dimension finale.

- Souflage
> Une boule de plastique rentre dans un moule ou celui-ci est souflé jusqu'à ce confondre avec le moule qui l'entour.
> C'est une technique que nous connaissons principalement pour la création de bouteuilles en plastique.

- Rotomoulage
> Technique où l'on verse les grains de plastique dans un moule qui est préalablement chauffé. Ce moule est sur un axe qui est en rotation et le plastique va foundre et prendre la forme de ce moule. Cette méthode donne un aspect rugueux à l'objet.
> Permet d''avoir une structure rigide et légère.

- Thermophormage
> Technique qui consiste à chauffer une feuille de plastique où une presse vient ensuite modeler le plastique.
> Nous obtenenons alors un objet fin et léger.


## Identification du plastique

Le centre de recherche en plastique de Gand à élaboré un kit contenant divers échantillons permettant de déterminer les plastiques en suivant un questionnaire qui se retrouve sur le [site](https://www.designmuseumgent.be/fr/collection/projet/plastiques) du centre de recherche de Gand. 
On y utilise le toucher, l'odorat et la vue afin de remplir ce questionnaire. Ce qui permettra de déterminer la famille de plastique sans devoir déteriorer l'objet.

Pour l'instant, les études sur le plastique commence doucement à émerger avec des formations que l'on peut, notamment, retrouver à La Cambre.

![](../images/Kit_plastique.JPG)

## La réserve et la conservation des plastiques

Pour ce qui esn est de la conservation des objets en plastiques, ceux-ci doivent tous être dans le noir afin d'éviter toute décoloration, dans une pièce fermée où chaque famille de plastique doit être séparée les unes des autres car ceux-ci émettent des gaz particuliers.
À noter, le PVC doit être conservé à l'écart car ce dernier émet, également, un liquide au fil du temps.

Dans cette réserve, on y retrouve quasiment la totalités des différents platiques. À savoir:
- ABS
- Fibre de verre (GRP)
- Polycarbonate (PC)
- Polyéthylène (PE)
- Polypropilène (PP)
- Polystyrène (PS)
- PMMA
- PUR
- PVC

## Useful links

- [C'est pas sorcier - Le plastique ça nous emballe](https://www.youtube.com/watch?v=irFEnEZhlNM)

- [Precious plastic](https://preciousplastic.com)


# Interface pour les enfants: premières idées

Histoire interactive en réalité augmentée où l'enfant sera d'une part plongé d'un monde et sera un acteur actif dans son apprentissage. 
