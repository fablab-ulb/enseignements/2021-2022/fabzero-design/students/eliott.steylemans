# Production de l'expérience AR

## Introduction du projet

Tout d'abord, nous avons réfléchi sur notre angle d'attaque sur ce que nous allions transmettre aux enfants lors de la visite au musée. Nous avons fait le choix de leur apprendre le passage d'un objet de base (tel, à titre d'exemple, une bouteuille d'eau) à un objet confectionné par un designer. Nous devons alors transmettre aux enfants le cheminement du designer et les choix qu'il a effectué afin d'atteindre son objectif pour concevoir son objet. 

Nous avons également du réfléchir au moyen dont nous allons transmettre ces informations. Nous nous sommes tournés vers la réalité augmentée. Ce moyen est, pour nous, l'évolution du guide audio que nous retrouvons aux musées.

Après avoir défni que nous allions travailler en réality augmentée, il était temps de concevoir notre idée et celle-ci s'est faite en plusieurs étapes.

### Création de l'histoire

Dans cette première étape, nous avons écrit un script pour le premier objet dont nous allons parler. Cet objet est la chaise Panton, de Verner Panton (1959).

![Script - Panton](./images/Script_Pantone.png)

### Concept art des personnages

Cette semaine, j'ai effectué un premier jet sur des idées de personnages. Dans mon interface en réalité augmentée, deux personnages racontront une histoire tout en transmettant les diverses informations concernant les objets et le musée. 

Ma vision dans la création des personnages est de créer des protagonistes qui seront attrayant pour les enfants tout en prennant en compte les contraintes techniques auxquelles je ferai façe lors de la création de ceux-ci.

Les critères de mes personnages sont:
- Une taille adaptée pour intéragir avec des enfants
- Attrayant et mignon qui donne confiance à l'enfant
- Limiter les parties mobile afin d'éviter un grand travail d'animation
- Visage en deux dimensions, à nouveau pour limiter le travail d'animation

Ci-suivant mes premiers concept arts de robots:
<p float="middle">
  <img src="./images/Concept_robot_v1_01.jpeg" width="330" />
  <img src="./images/Concept_robot_v1_02.jpeg" width="330" /> 
  <img src="./images/Concept_robot_v1_03.jpeg" width="330" />
</p>

Enfin, voici les deux robots que j'ai sélectionnés pour cette première version:

<p float="middle">
  <img src="./images/Concept_robot_v1_06.jpeg" width="500" />
  <img src="./images/Concept_robot_v1_07.jpeg" width="500" /> 
</p>

Une petite vidéo montrant l'évolution du robot "A" lorsque je le dessinais:

![RobotA_v01](./images/Concept_robot_v1_A.GIF)

### Storyboard

Ci-dessous, un première exemple d'histoire reprennant nos deux protagonistes qui expliquent les fauteuils gonflables.


<p float="middle">
  <img src="./images/Concept_storyboard_v1_01.PNG" width="500" />
  <img src="./images/Concept_storyboard_v1_02.PNG" width="500" /> 
  <img src="./images/Concept_storyboard_v1_03.PNG" width="500" />
  <img src="./images/Concept_storyboard_v1_04.PNG" width="500" />
  <img src="./images/Concept_storyboard_v1_05.PNG" width="500" />
  <img src="./images/Concept_storyboard_v1_06.PNG" width="500" />
</p>

![Storyboard_Gonflable](./images/Concept_storyboard_v1_A.GIF)


### Aspects technique

Pour concevoir le projet, nous avons mené une recherche sur les logiciels que nous allions utilisé pour la modélisation et l'animation ainsi que le platforme sur laquelle l'expérience en réalité fonctionnera.

Nous avons décidé ici d'utiliser Blender comme logiciel principal. Blender est un logiciel Open Source, disponible sur MacOS, Windows et Linux et permet de faire un grand nombre de choses tel que la modelisation et l'animation qui nous intéresse. Nous l'avons selectionné car il est gratuit et donc accessible à tous, permet de faire tout ce que l'on a besoin et parce que j'ai, personnellement, eu l'occasion de l'utiliser pour différents projet et j'ai donc déjà acquéri une base qui me permet de mieux comprendre ce qu'il est nécessaire de faire et le temps que les différentes tâches pourraient nous prendre.

Pour ce qu'il en est de l'AR, plusieurs framework existent tels que ARkit, ARCore, HoloLens ou encore MagicLeap pour citer les plus grands. 
Nous avons fait le choix d'utiliser ARkit fourni par Apple. Le choix de travailler uniquement sur ARKit est car Apple a mis en place différents outils comme Reality Composer qui nous permet de créer une expérience en AR sans à coder les différentes actions que nous mettrions dans cette expérience. Ou encore ARkit Scanner qui nous permet de scanner un objet en 3 dimensions pour pouvoir anchrer notre scéne à un objet de la réalité afin de simplifier la vie de l'utilisateur et assurer que tout se positionne correctement. Ce framework est uniquement compatible avec des appareils iOS.

### Modélisation

Comme je l'ai préalablement évoqué, la modélisation s'est passée au travers de Blender. La forme de départ pour modéliser Plastico était un cube. J'ai placé le concept art de Plastico en fond et j'ai ensuite manipulé ce cube de base pour y obtenir la forme finale de Plastico. J'y ai ensuite ajouté ses bras, son visage et ses accessoires.
Pour ce qu'il en est des chaises, la chaise "Pantone" peut être trouvé sur le site officiel de Vitra et les chaises d'écoles sont des modèles (CC Attribution) trouvées sur [Sketchfab](https://sketchfab.com/3d-models/school-chair-d77a0e107a764dff9be2b1d23fd352db).

![Plastico](./images/Plastico_render.png)


### Animation

Afin de pouvoir animer Plastico, nous devons lui faire un "squelette" ou une armature. 
Ceci permet de controler notre personnage bien plus aisément et permet également de ne pas modifier l'objet en lui même. Ceci permet également de connecter les différents "os" tout au long du personnage et d'avoir des jonctions plus naturelles ainsi qu'un travail d'animation plus léger. Exemple: lorsque nous glissons notre genou vers un coté, notre bassin, jambe et cheville pivote avec pour accompagner le mouvement. Ce type de mouvement peut être controllé grâce à notre armature.

Les objets sont, quant à eux, directement animés sans avoir besoin de créer un squelette.

<p float="middle">
  <img src="./images/Plastico_rig1.png" width="500" />
  <img src="./images/Plastico_rig2.png" width="500" /> 
</p>

![Plastico - Animation](./images/Plastico_animation1.png)

Difficultés qui persistent:
- Impossibilité de faire apparaitre de nouveaux objets, il faut alors les placer en dehors du champs de vision de l'utilisateur et le déplacer quand il est temps de l'apercevoir 
- Déformation des les bras de Plastico lorsque nous essayons de rendre son bras extensile afin qu'il puisse prendre des objets en mains
- Certains objets ne se positionnent pas correctement dans la scène

### Intégration dans Reality Composer 

Reality Composer est une application développée par Apple afin de créer des expériences AR sans à devoir coder. Bien entendu, l'application est alors limitée par le fait qu'aucun code n'est nécessaire à créer une expérience mais elle en reste tout de même fournie en action que l'on peut réaliser et répond amplement à nos besoins. Cette application est disponible sur MacOS, iOS et iPadOS. Ce qui permet de travail sur son appareil mobile et d'y avoir un aperçu en temps réel.

![Reality Composer](./images/Screenshot_RealityComposer1.png)

Seulement, les objets et animations que Reality Composer doivent être dans un format .USDZ (Pixar’s Universal Scene Description standard) qui est un très bon format de fichier reprennant Objets, textures et animations tout en un. Cependant, Blender ne permet pas d'exporter directement notre scène en .usdz. Il me faut alors l'exporter en .gltf et transverser le fichier dans Reality Converter - autre outil développé par Apple pour convertir certains format de fichier en .usdz - pour ensuite pouvoir l'importer dans notre projet Reality Composer.

![Reality Converter](./images/Screenshot_RealityConverter.png)

Une fois que tout est positionné où nous le souhaitons, il ne faut plus que l'exporter sur notre téléphone et celui-ci se charge du reste.



### Résultats du test au fablab

Nous avons eu l'occasion de tester notre projet au fablab où les étudiants (6-7 ans) de l'école Decroly sont venus afin d'essayer nos projets respectifs.
Dans l'ensemble, nous sommes très content des résultats que nous avons eus. Les enfants étaient concentrés lors de la visite, se sont amusés (nous avons eu droit à quelques rires) et ont extrèmement bien retenu ce que nous leurs avons transmis. Si bien que le jeu que nous avions prévu après la visite afin de les questionners sur des sujets similaires pour approfondir le sujet (voir le Git de Zoé et/ou Camille pour plus d'informations) n'était en fait pas du tout nécessaire car les enfants savaient tout nous retransmettre même avant de commencer ce dernier.

Ce que nous avons également remarqué au fur et à mesure de la journée, est que les enfants veulent avoir un petit souvenir du musée et de Plastico. Nous avons alors pensé à la création d'une "Sharing Box" à la fin de la visite permettant de prendre une photo souvenir avec Plastico et des objets du musée.

<p float="middle">
  <img src="./images/Photo-fablab1.JPG" width="350" />
  <img src="./images/Photo-fablab2.JPG" width="350" /> 
  <img src="./images/Photo-fablab3.JPG" width="350" /> 
</p>


### Résumé visite des enfants 

| Ce qui a marché       | Ce qui n'a pas marché           |
| ------------- |:-------------:|
| C'est quoi le design? sac à dos, usage, signe    | Inventer des projets |
| Liens avec ce qu'ils connaissent (chantilly -> impression 3d)     | Nom des objets     |
| Points communs / différences | Durée de la visite     |
| Dessins des chaises |      |
| Déconstruire les objets |      |
| Même hauteur avec les enfants |      |
| Comparaison |      |
| Mémoire des enfants |      |


## Suite du projet, direction pre-jury

Pour la suite du projet, nous avons pris note de ce crash-test et nous allons continuer le développement de notre projet. Nous avons décidé de continuer à se départager le travail avec la partie informatique de mon côté et la création des histoires et de la partie physique pour les filles. Je vous invite alors à consulter leur Git pour la lecture du projet au complet.

[Git de Camille](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-design/students/camille.brianchon/)
[Git de Zoé](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-design/students/zoe.verwilghen/)

### Réflexion sur les audio-guide


| Analyse audio-guide       |          |
| ------------- |:-------------:|
| Banksy       | Une minute en moyenne par oeuvre, déconstruction de l'oeuvre ainsi que son contexte et apport d'explications, très compliqué pour un enfant         |
| Kaserne      | Lenteur dans la narration à cause de terme très compliqué         |
| Focus on furniture        | Enregistrement de mauvaise qualité         |

Remarques négatives générales:
- Pas de narration et aucun fil conducteur, on passe d'une oeuvre à une autre en arretant à chaque fois l'audio-guide et en devant indiquer à nouveau le numéro de l'oeuvre
- Compliqué de remettre l'objet dans son contexte
- Language trop compliqué pour un enfant
- Aucune interraction avec le visiteur


### Avancement dans l'animation 

Comme nous avons eu l'approbation sur l'expérience que nous donné aux enfants, je peux continuer l'animation de notre histoire. J'ai alors commencé la 3ème et finale partie. 
Conçernant les problèmes énoncés auparavent, la position éronnée des objets a été résolue mais les deux autres restent encore présents. Je ne trouve actuellemt pas de solution pour la déformation du bras de Plastico. Lorsque je travaille sur l'animation dans Blender, tout est correcte mais c'est au momement de l'exportation que la deformation a lieue. Je ne trouve donc pas de raison au problème malgré de nombreuses recherches et de temps passé à troubleshooter la question.
Le problème conçernant l'impossibilité de faire apparaitre de nouveaux objets n'est, quan† à lui, pas dérangeant et ne pose aucun problème. Je l'ai donc laissé de cette manière car cela me permet de travailler plus aisément, même si une solution existe.

### Création du photomaton ou de la "Sharing box"

Nous avons décidé de créer cette sharing box en y intégrant Plastico en réalité augmentée afin de prendre une photo avec lui en fin de visite du muséê.

L'idée est la suivite :
- L'enfant séléctionne sur l'écran un ou plusieurs objets de la collection du musée qui s'afficheront en AR sur la photo
- Les objets apparaissent ainsi que Plastico
- La photo est prise
- La photo s'imprime 

J'ai donc créée un nouveau projet sur Reality Composer pour créer l'expérience AR et je me suis rapidement rendu compte l'idée complète n'allait pas être réalisable. En effet, Reality Composer est un outil qui tient sa force dans sa simplicité mais qui en est aussi son défaut. L'outil ne permet pas d'apporter un peu de complexité dans un projet et ne permet donc pas d'y intégrer une liste d'objets à sélectionner, de les ajouter à la scène pour finalement imprimer la photo. Je suis également limité par la technologie actuelle qui permet uniquement, lors de l'utilisation de la caméra avant (puisque l'enfant doit pouvoir se voir afin de prendre la photo), de suivre un seul visage. Je ne peux donc pas voir le sol et positionner mes objets en fonction.

Nous avons alors pris la décision de limiter cette idée à une photo avec Plastico seulement.


La fabrication de la boite a été réalisée par les filles.

## Application 

J'ai également commencé à développer mon application et ~~ uploadé le code source dans un nouveau repository que vous retrouverez [Ici](https://gitlab.com/eliottst/Museum)(est pour l'instant en privé).
Dans le cas où j'aurais besoin d'aide, ce repository permettra à la personne en question d'accèder directement au code et d'y proposer des modifications. ~~

Cette application a pour but de gérer l'intégration des expériences en AR avec la visite du muséê.
L'application sera composée en trois sections avec une page d'acceuil introduisant le fonctionnement de l'application et les règles à respecter dans le musée + les différents axes de visites et, par la suite, une seconde section où se trouvera la collection du musée placé dans leur catégorie respectives où l'on peut y lire une description et placer les objets en AR chez soi ainsi qu'un troisème reprennant des infos pratique concernant le musée.

### Première version de la page d'acceuil
<p float="middle">
  <img src="./images/Simulator_Screen_walkthrough_v1_1.png" width="330" />
  <img src="./images/Simulator_Screen_walkthrough_v1_2.png" width="330" /> 
  <img src="./images/Simulator_Screen_walkthrough_v1_3.png" width="330" />
</p>


## Pre-jury

Cette journée a très mal commencée lorsque nous nous sommes aperçus, malgrés plusieurs essais au préalable, que notre expérience ne marchait plus du tout.

Je m'explique. L'application a été créée dans une scène où j'y avais placé une chaise Panton et le socle sur lequel la chaise est positionnée. Le plan était alors que l'expérience se lance une fois la chaise, dans le monde réel, est reconnue. La scène entière est alors affichée selon les coordonnées de l'objet scanné. Seulement, la position de la chaise Panton dans le monde virtuel, ne correspondait pas exactement à sa position dans le monde réel. Ce qui a eu comme conséquence que la scène soit entièrement décalée. De plus, la reconnaissance de l'objet est particulièrement difficile à cause du fait que la chaise réfléchie de manière importante la lumière et l'appareil n'arrive pas alors à percevoir ce dernier.

Il a alors fallu travailler quelques minutes pour retourner sur la méthode utilisée précedemment, à savoir: de placer manuellement la scène sur le sol et d'espérer pour le mieux que celle-ci s'alligne.

Malheureusement, nous atteignons la limite de ce que peut offrir Reality Composer et d'autres problèmes ont également fait surface:
- L'application est extrémement lourde et prend une place dans la mémoire vive (RAM) du téléphone ce qui entrainait celle-ci a crasher de manière aléatoire ou même, de ne pas du tout s'ouvrir;
- L'appareil perd son point de repère à un moment donné durant l'expérience et demande de rescanner la pièce. Nous avons trouvé aucune raison expliquant ce problème.


Malgré ce debut de journée, ces problèmes n'ont pas été aussi handicapants que nous le pensions et les retours ont été extrémement positifs et de nombreuses remarques pouvant améliorer le projet ont été énoncées.


Voici ci-dessous les remarques que nous avons receuillies: 

- Insister sur la diffusion

- Trouver un support pour y placer le telephone 

- Communiquer l’expérience 

- Faire story board pour chaque objet en développant les animations

- Faire une deuxième AR dans un autre angle d’attaque ?

- Faire parler les objets plutot que plastico ?

- Designer un objet plutôt qu’un robot ?

- Note explicative derrière la photo en fonction de la visite qu’il a fait ou de l’objet qu’il a choisit

- Un peu trop long, la question du support 

- Question du parcours à détailler

- Analyser les questions pratiques (exemple: iPad du musée ou personnel?)

- Finir tout le scénario 

- Illustrer le porte à faux ou superposé un S dessus 

- Possibilité que l’enfant puisse s’approcher ?

- Peut être seulment 30 secondes pour montrer une caractéristique importante

<p float="middle">
  <img src="./images/PhotoBooth_01.jpeg" width="350" />
  <img src="./images/PhotoBooth_02.jpeg" width="350" /> 
  <img src="./images/PhotoBooth_03.jpeg" width="350" /> 
</p>

## Dernière ligne droite...

# Transition du projet vers une nouvelle platforme

Bien que nos démos ont "fonctionnées" jusqu'à lors très bien avec reality composer, nous nous sommmes rendu compte que la platforme reste limitée en beaucoup de points et ne peut pas être utilisée pour projet de notre envergure. 

Les défauts notables que nous avons relevés sont :
- Les problèmes que nous avons eu lors du pré-jury;
- Fonctionne uniquement sur iOS (iPad+iPhone) ;
- Tracking limité (tracking des objets particulièrement difficiles lorsque ceux-ci réfléchissent de la lumière)  ;
- La transition entre chaque objets de la visite n'est pas possible (pour cause, encore une fois, du tracking limité. Chaque utilisateur devrait aligner de manière manuelle la scène. Ce qui n'est pas souhaitable) ;
- Interactions limitées et l'apport de nouvelles visites, fonctionnalités,... compliquées.

Pour toutes ces raisons, nous avons pris la décision de transitionner le projet vers un nouveau moteur afin de fournir un projet final qui pourrait être réalisé immédiatement et qui permettrait de mieux convaincre de potentiels investisseurs.

La platforme sur laquelle nous allons maintenant travailler est Unity. Unity est un moteur de jeu multi-platforme et est l'un des plus utilisés dans l'industrie du jeu vidéo. 
Unity va nous permettre de résoudre tout les problèmes évoqués ci-dessus, d'obtenir une meilleure flexibilité de conception et d'évoluer le projet en même rythme que nos idées.


### Choix du Framework

Un Framework est un logiciel qui est développé par et pour les développeurs pour créer des applicatioins. Autrement-dit, c'est une base sur lequel s'appuie le développeur lui permettant de se concentrer sur des tâches plus importantes.

Dans notre cas, nous devons sélectionner le Framework qui va être la base de fonctionnement pour notre expérience de réalité augmentée.

Il existe de nombreux Framework permettant de travailler en réalité augmentée mais deux parmis eux ont retenu mon attention: le premier est "AR Foundation" qui est un framework développé directement par Unity et est une intégration multiplatforme native. Ce framework est un excellent candidat mais n'est finalement pas celui qui a été retenu. C'est le framework "Vuforia" que j'ai finalement choisi car celui-ci permet aussi s'intégrer nativement sur différentes platformes et même, de supporter des appareils en plus que AR Foundation mais permet surtout un tracking des pièces intérieurs bien plus simplifiés que ce que propose le premier.

L'unique barrière que pose Vuforia est le passage obligatoire à la caisse lors d'une intégration dans une autre application. Dans notre cas, c'est l'intégration de notre expérience avec l'application du musée. Nous aurons donc deux applications distinctes lors de notre présentation.

### Développement de l'application du musée

J'ai continué à développer la vision que nous avions eue auparavant. Celle-ci fonctionne réellement (n'est pas publiée su l'app store pour des raisons de licence) et y intègre toutes les fonctionnalités que nous voulions intégrer.

L'application fonctionne comme ceci:
- Ouverture de l'application
- Première carte "Design museum brussels" qui comporte une vidéo explicative de l'application ainsi que les règles de bonne conduite
- Sélection de l'axe d'apprentissage 
- Lancement de l'expérience après avoir scanner le code qr se trouvant devant l'objet dans le muséê

- Onglet "Collection" permettant d'en lire un peu plus sur les objets du musée et les ramener chez soi en AR

La visite est complétement libre avec la possibilité de voir autant d'objets que l'on veut et de passer d'une catégorie à une autre lorsqu'on le souhaite.


<p float="middle">
  <img src="./images/Application_01.PNG" width="330" />
  <img src="./images/Application_02.PNG" width="330" /> 
  <img src="./images/Application_03.PNG" width="330" />
</p>

### Pour aller plus loin...

Le framework utilisé peut tout à fait convenir pour l'expérience finale et est un très bon choix pour faciliter la vie du développeur grâce à sa bonne documentation, ses outils mis à disposition et sa compatibilité avec les différents appareils de réalité augmentée.
Toute fois, Vuforia reste payant (le prix m'est inconnu, une demande de devis est nécessaire pour en savoir plus) et celui-ci reste limité à un usage local avec l'application du musée à installer pour chaque utilisateur.

Il est peut être préférable, alors, de choisir une solution gratuite et qui permettrait dans un futur de se lancer sans le besoin de télécharger l'application spécifique. Il est en effet possible d'utiliser ce qu'on appelle des "Cloud anchors" qui permet à l'appareil AR de reconnaitre le lieu où il se trouve et de lancer l'expérience qui lui est attaché. Ces "cloud anchors" permettent également de synchroniser les expériences avec plusieurs utilisateurs. On pourrait alors y ajouter une fonction qui permettrait de lancer la visite pour un groupe et ceux-ci verront tous la même chose au même moment et peuvent intéragir entre eux.

Pour ce qui en est des visites, l'utilisation de Unity nous permet maintenant très facilement de rajouter de nouvelles animations, interactions,.. ou de les modifier. 

Pour finir, l'intégration de l'expérience avec l'application peut être résolue soit en payant la license Vuforia, soit en utilisant les "Cloud anchors" présentés ci-dessus.



[Git de Camille](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-design/students/camille.brianchon/)
[Git de Zoé](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-design/students/zoe.verwilghen/)