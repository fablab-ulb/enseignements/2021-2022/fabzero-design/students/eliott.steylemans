## About me...

Hello! 
Je m'appelle ****Eliott**** et je suis en Master 2 à l'école d'architecture ULB La Cambre Horta.

Pourquoi ai-je pris cette option "Architecture et design" (module3)? Et bien parce que le design est quelque chose qui m'intéresse maintenant depuis un petit temps et que je n'ai jamais eu l'occasion d'étudier lors de mon bachelier en architecture à Loci Bruxelles. L'ULB m'offrant enfin cette opportunité, je suis ravi de rejoindre cette option!

![Moi, dans une carrière d'argile](images/Profile.jpg)
_Petite photo souvenir dans une carrière d'argile avec une splandide tenue..._



## Mes passions

Depuis ma plus tendre enfance, je me suis toujours intéressé par les nouvelles technologies. Depuis mon enfance où j'ai découvert l'ordinateur chez mes grands parents, je n'ai cessé de m'intéresser au domaine. Aujourd'hui, je suis fort intéressé par la réalité augmentée et la réalité virtuelle ainsi que la fabrication digitale. 

La musique est un point qui m'intéresse fortement également. Je me suis équipé d'un petit matériel Hifi et je passe mes soirees a ecouter de la musique. 
